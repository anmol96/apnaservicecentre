﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ApnaServiceCentre.Startup))]
namespace ApnaServiceCentre
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
